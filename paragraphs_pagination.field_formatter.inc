<?php

/**
 * @file
 * Hooks and functions relevant to the paragraph pagination field formatter.
 */

/**
 * Implements hook_field_formatter_info().
 */
function paragraphs_pagination_field_formatter_info() {
  return array(
    'paragraphs_paginated_view' => array(
      'label' => t('Paragraphs paginated items'),
      'field types' => array('paragraphs'),
      'settings' =>  array(
        'view_mode' => 'full',
        'ajax_enabled' => FALSE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function paragraphs_pagination_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $elements = array();

  if ($display['type'] == 'paragraphs_paginated_view') {

    $entity_type = entity_get_info('paragraphs_item');
    $options = array();
    foreach ($entity_type['view modes'] as $mode => $info) {
      $options[$mode] = $info['label'];
    }

    $elements['view_mode'] = array(
      '#type' => 'select',
      '#title' => t('View mode'),
      '#options' => $options,
      '#default_value' => $settings['view_mode'],
      '#description' => t('Select the view mode'),
    );

    $elements['ajax_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Ajax'),
      '#default_value' => $settings['ajax_enabled'],
      '#description' => t('Enable ajax'),
    );
  }

  return $elements;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function paragraphs_pagination_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $output = array();

  if ($display['type'] == 'paragraphs_paginated_view') {
    $entity_type = entity_get_info('paragraphs_item');
    if (!empty($entity_type['view modes'][$settings['view_mode']]['label'])) {
      $output[] =  t('View mode: @mode', array('@mode' => $entity_type['view modes'][$settings['view_mode']]['label']));
    }
    else {
      $output[] = ' ';
    }
  }

  return implode('<br>', $output);
}

/**
 * Implements hook_field_formatter_view().
 */
function paragraphs_pagination_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  if ($display['type'] === 'paragraphs_paginated_view') {
    $content = array();

    // Prevent display if empty.
    if (empty($items)) {
      return $element;
    }

    // Initialize current page.
    $current_page_number = 0;

    // Get page_break bundle name.
    $page_break_bundle = variable_get('paragraphs_pagination_page_break_bundle', '');

    // If no_paging query param is present, do not render the pager.
    $no_paging = _paragraphs_pagination_no_paging_present();

    // Get the ajax enabled property.
    $ajax_enabled = empty($display['settings']['ajax_enabled']) ? FALSE : $display['settings']['ajax_enabled'];
    
    // Get view mode from entity.
    $display_view_mode = empty($display['settings']['view_mode']) ? 'full' : $display['settings']['view_mode'];

    // Get view mode from field instance (if configured).
    $view_mode = empty($instance['display'][$display_view_mode]['settings']['view_mode']) ?
      $display_view_mode :
      $instance['display'][$display_view_mode]['settings']['view_mode'];
    
    // Get a unique identifier for our wrapper (use existing on Ajax callback).
    $id = isset($entity->paragraphs) && isset($entity->paragraphs['dom_id']) ? 
      $entity->paragraphs['dom_id'] : drupal_html_id('id');
    $selector = 'paragraphs-pagination-wrapper-' . $id;
    
    $element['#theme_wrappers'] = array('paragraphs_items');
    $element['#attributes']['class'][] = drupal_clean_css_identifier('paragraphs-items');
    $element['#attributes']['class'][] = drupal_clean_css_identifier('paragraphs-items-view-mode-' . $view_mode);
    $element['#attributes']['class'][] = drupal_clean_css_identifier('paragraphs-items-field-' . $instance['field_name']);
    $element['#view_mode'] = $view_mode;

    if ($no_paging) {
      // Remove page_break bundles from render array.
      foreach ($items as $delta => $item) {
        $paragraph = paragraphs_field_get_entity($item);

        if ($paragraph && $paragraph->bundle <> $page_break_bundle) {
          $content[$delta] = $paragraph;
        }
      }
    }
    else {
      // Splice our paragraph bundles into pages.
      $pages = _paragraphs_pagination_splice_bundles($items, $page_break_bundle);
      $page_total = count($pages);
      $current_page_number = pager_default_initialize($page_total, 1);

      if (isset($pages[$current_page_number])) {
        $content = $pages[$current_page_number];
      }
      else {
        // This page does not exist!
        drupal_not_found();
      }
    }

    // Render our visible paragraph bundles and add them to the render array.
    foreach($content as $delta => $paragraph) {
      if (entity_access('view', 'paragraphs_item', $paragraph)) {
        $element[$delta]['entity'] = $paragraph->view($view_mode);
      }
    }

    // Attach our JS for AJAX paging.
    if ($ajax_enabled) {
      $element['#attached']['library'][] = array('system', 'drupal.ajax');
      $element['#attached']['js'][] = drupal_get_path('module', 'paragraphs_pagination')
        . '/js/paragraphs_ajax.js';
      $element['#attached']['js'][] = array(
        'type' => 'setting',
        'data' => array(
          'paragraphs' => array(
            'ajaxPagination' => array(
              $id => array(
                'ajaxCallbackUrl' => url('ajax/paragraphs'),
                'currentPage' => $current_page_number,
                'displayMode' => $display,
                'entityType' => $entity_type,
                'entityId' => $entity->nid,
                'fieldName' => $field['field_name'],
                'id' => $id,
                'selector' => '#' . $selector,
              ),
            ),
          ),
        ),
      );

      // Add Ajax overlay settings.
      $use_ajax_overlay = variable_get('paragraphs_pagination_ajax_overlay_default', TRUE);
      if ($use_ajax_overlay) {
        $element['#attached']['css'][] = drupal_get_path('module', 'paragraphs_pagination')
          . '/css/paragraphs_ajax_overlay.css';
        $element['#attached']['js'][] = drupal_get_path('module', 'paragraphs_pagination')
          . '/js/paragraphs_ajax_overlay.js';
        $element['#attached']['js'][] = array(
          'type' => 'setting',
          'data' => array(
            'paragraphs' => array(
              'ajaxOverlay' => array(
                'message' => check_plain(variable_get_value('paragraphs_pagination_ajax_overlay_message')),
              ),
            ),
          ),
        );
      }
    }
    
    if (!$no_paging && $page_total > 1) {
      // Render our pager.
      $args = array(
        'quantity' => $page_total,
      );

      $element['#pager'] = TRUE;
      $element['#prefix'] = '<div id="' . $selector . '" class="paragraphs-pagination-wrapper">';
      $element['#suffix'] = theme('pager', $args) . '</div>';
    }
  }
  return $element;
}
