/**
 * @file
 * Handles AJAX fetching of paragraphs items.
 */
(function ($) {

  Drupal.behaviors.paragraphsajaxPagination = {};
  Drupal.behaviors.paragraphsajaxPagination.attach = function(context, settings) {
    if (settings && settings.paragraphs.ajaxPagination) {
      $.each(Drupal.settings.paragraphs.ajaxPagination, function(i, paragraphSettings) {
        Drupal.paragraphs.instances[i] = new Drupal.paragraphs.ajaxPagination(paragraphSettings);
      });
    }
  };

  Drupal.paragraphs = Drupal.paragraphs || {};
  Drupal.paragraphs.instances = [];

  Drupal.paragraphs.ajaxPagination = function(settings) {
    var ajaxCallbackUrl = settings.ajaxCallbackUrl,
        queryString = window.location.search || '',
        selector = settings.selector;

    // Store our paragraphs.
    this.$paragraphs = $(selector);

    // Check if there are any GET parameters to include in the AJAX call.
    if (queryString !== '') {
      // Remove the question mark and Drupal path component if any.
      queryString = queryString.slice(1).replace(/q=[^&]+&?|&?render=[^&]+/, '');

      if (queryString !== '') {
        // If there is a '?' in ajax_path, clean url are on
        // and & should be used to add parameters.
        queryString = ((/\?/.test(ajaxCallbackUrl)) ? '&' : '?') + queryString;
      }
    }

    // Save our settings.
    this.settings = settings;

    // Initialize and save settings for Drupal.Ajax magic.
    this.elementSettings = {
      url: ajaxCallbackUrl + queryString,
      submit: settings,
      setClick: true,
      event: 'click',
      selector: selector,
      progress: { type: 'throbber' }
    };

    // Add the Ajax to the pager.
    this.$paragraphs.once($.proxy(this.attachPager, this));

    // We support the paragraphs_table_of_contents module.
    // Add the Ajax to the toc links.
    this.$toc = $('.paragraphs-item-table-of-contents');
    if (this.$toc.length) {
      this.$toc.once($.proxy(this.attachTableOfContents, this));
    }
  };

  /**
   * Attach the ajax behavior to all pager links.
   */
  Drupal.paragraphs.ajaxPagination.prototype.attachPager = function() {
    this.$paragraphs.find('ul.pager > li > a').each(
      $.proxy(this.attachLink, this)
    );
  };

  /**
   * Attach the ajax behavior to all toc links.
   */
  Drupal.paragraphs.ajaxPagination.prototype.attachTableOfContents = function() {
    this.$paragraphs.find('li > a').each(
      $.proxy(this.attachLink, this)
    );
  };

  /**
   * Attach the ajax behavior to a singe link.
   */
  Drupal.paragraphs.ajaxPagination.prototype.attachLink = function(id, link) {
    var ajaxData = {},
        $link = $(link);

    // Only attach the Ajax behavior if linking to another page.
    if (this.settings.currentPage !== this.getPage($link.attr('href'))) {
      $.extend(
        ajaxData,
        this.settings,
        {'originalPath': $link.attr('href')}
      );

      this.elementSettings.submit = ajaxData;
      new Drupal.ajax(false, $link, this.elementSettings);
    }
  };

  /**
   * Helper function to retrieve the page number from a given url.
   */
  Drupal.paragraphs.ajaxPagination.prototype.getPage = function(url) {
    var pair,
        querystring;

    if (url.indexOf('?') !== -1) {
      querystring = url.substring(url.indexOf('?')+1).split('&');

      for (var i = 0; i < querystring.length; i++) {
        pair = querystring[i].split('=');

        if (pair[0] === 'page') {
          return pair[1];
        }
      }
    }

    return 0;
  };

  /**
   * Scroll to correct anchor after paragraphs have been loaded.
   */
  Drupal.ajax.prototype.commands.paragraphsPaginationScrollTo = function(ajax, response, status) {
    $('body, html').animate({
      scrollTop: $(response.selector).offset().top
    }, 500);
  };

})(jQuery);
