/**
 * @file
 * Enables the ajax loading overlay before the actual Ajax call.
 */

(function ($) {
  var beforeSend = Drupal.ajax.prototype.beforeSend,
      success = Drupal.ajax.prototype.success,
      error = Drupal.ajax.prototype.error;

  Drupal.ajax.prototype.beforeSend = function(xmlhttprequest, options) {
    // Add loading overlay on all ajax-enabled paragraphs.
    if ($(this.selector).hasClass('paragraphs-pagination-wrapper')) {
      Drupal.behaviors.paragraphsAjaxOverlay.loadAjaxOverlay($(this.selector));
    }

    beforeSend.call(this, xmlhttprequest, options);
  };

  Drupal.ajax.prototype.success = function(response, status) {
    // Fail-safe to remove loading overlay on all ajax-enabled paragraphs.
    Drupal.behaviors.paragraphsAjaxOverlay.removeAjaxOverlay($(this.selector));

    success.call(this, response, status);
  };

  Drupal.ajax.prototype.error = function(response, status) {
    // Fail-safe to remove loading overlay on all ajax-enabled paragraphs.
    Drupal.behaviors.paragraphsAjaxOverlay.removeAjaxOverlay($(this.selector));

    error.call(this, response, status);
  };

  Drupal.behaviors.paragraphsAjaxOverlay = {
    $overlay: $('<div class="loading-overlay">' +
      '<div class="loader">' +
      '<div class="loader__animation"></div>' +
      '<div class="loader__message"></div></div></div>'),

    attach: function (context, settings) {
      if (settings.paragraphs.ajaxOverlay && settings.paragraphs.ajaxOverlay) {
        this.$overlay.find('.loader__message').text(settings.paragraphs.ajaxOverlay.message);
      }
    },

    /**
     * Appends the overlay to a given element.
     *
     * @param $element
     */
    loadAjaxOverlay: function ($element) {
      var offsetY,
          $loader = this.$overlay.children('.loader');

      // Reset inline styles.
      $loader.removeAttr('style');

      offsetY = this.getElementViewPortCenter($element);

      $loader.css('top', offsetY);
      $element.prepend(this.$overlay);
    },

    /**
     * Removes the overlay from given element.
     *
     * @param $element
     */
    removeAjaxOverlay: function ($element) {
      if ($element.hasClass('paragraphs-ajax-overlay')) {
        $element.find('.loading-overlay').remove();
      }
    },

    /**
     * Helper function to get the center of an element within the viewport.
     *
     * @param $element
     * @returns {*}
     */
    getElementViewPortCenter: function ($element) {
      var scrollTop = $(window).scrollTop(),
        scrollBot = scrollTop + $(window).height(),
        elHeight = $element.outerHeight(),
        elTop = $element.offset().top,
        elBottom = elTop + elHeight,
        elTopOffset = elTop < scrollTop ? scrollTop - elTop : 0,
        elBottomOffset = elBottom > scrollBot ? scrollBot - elTop : elHeight;

      // Return 50% if entire element is visible.
      if (elTopOffset === 0 && elBottomOffset === elHeight) {
        return '50%';
      }

      return Math.round(elTopOffset + ((elBottomOffset - elTopOffset) / 2)) + 'px';
    }

  };

})(jQuery);
