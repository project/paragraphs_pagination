README.txt
==========

Here how's to enable pagination for your paragraphs:
1) Go to the display settings for a given content type and select
   'Paragraphs Pagination' as the formatter for your paragraphs field. Optionally
   you can also enable AJAX pagination.
2) Ensure you have a 'page_break' paragraph bundle by navigating to
   /admin/structure/paragraphs.
3) Create a node with some paragraph content, and add a couple of page breaks.
4) See the results by viewing the published content.
