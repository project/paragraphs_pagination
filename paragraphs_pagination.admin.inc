<?php

/**
 * @file
 * Module configuration file.
 */

/**
 * Form callback for module configuration.
 */
function paragraphs_pagination_settings_form($form_state) {
  $form = array();
  $bundles = paragraphs_bundle_load();
  $options = array('' => t('None'));
  foreach ($bundles as $machine_name => $bundle) {
    $options[$machine_name] = $bundle->name;
  }

  $form['paragraphs_pagination_page_break_bundle'] = array(
    '#type' => 'select',
    '#title' => t('Page Break Bundle.'),
    '#description' => t('Change this value to whichever bundle you would like to
        act as page break when pagination is enabled.'),
    '#options' => $options,
    '#default_value' => variable_get('paragraphs_pagination_page_break_bundle', ''),
  );

  return system_settings_form($form);
}
