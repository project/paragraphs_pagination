<?php

/**
 * @file
 * Module configuration variables.
 */

/**
 * Implements hook_variable_info().
 */
function paragraphs_pagination_variable_info($options) {
  $bundles = paragraphs_bundle_load();
  $options = array('' => t('None'));
  foreach ($bundles as $machine_name => $bundle) {
    $options[$machine_name] = $bundle->name;
  }

  $variables['paragraphs_pagination_page_break_bundle'] = array(
    'type' => 'string',
    'element' => array(
      '#type' => 'select',
      '#options' => $options,
    ),
    'title' => t('Page Break Bundle.', array(), $options),
    'description' => t('Change this value to whichever bundle you would like to
        act as page break when pagination is enabled.', array(), $options),
    'default' => '',
  );

  $variables['paragraphs_pagination_ajax_overlay_enabled'] = array(
    'type' => 'boolean',
    'title' => t('Enable overlay effect.', array(), $options),
    'description' => t('When enabled, all paragraphs with ajax will show the overlay.', array(), $options),
    'default' => FALSE,
  );

  // Paragraphs Ajax Overlay message.
  $variables['paragraphs_pagination_ajax_overlay_message'] = array(
    'type' => 'string',
    'title' => t('Paragraphs Pagination Overlay message', array(), $options),
    'description' => t('The message to show when the overlay is enabled.', array(), $options),
    'required' => TRUE,
    'localize' => TRUE,
    'default' => 'Loading...',
  );

  return $variables;
}
